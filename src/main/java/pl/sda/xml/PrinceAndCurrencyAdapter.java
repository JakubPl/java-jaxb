package pl.sda.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigDecimal;

public class PrinceAndCurrencyAdapter extends XmlAdapter<String, PriceAndCurrency> {
    @Override
    public PriceAndCurrency unmarshal(String priceString) throws Exception {
        String currencySign = priceString.substring(0, 1);
        String amount = priceString.substring(1);
        return new PriceAndCurrency(currencySign, new BigDecimal(amount));
    }

    @Override
    public String marshal(PriceAndCurrency priceAndCurrency) throws Exception {
        return priceAndCurrency.getCurrencySign() + priceAndCurrency.getAmount();
    }
}
