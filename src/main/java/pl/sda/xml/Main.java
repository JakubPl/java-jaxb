package pl.sda.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws JAXBException {
        //orginalny XML pochodzi z https://www.w3schools.com/xml/plant_catalog.xml

        InputStream plantStream = Main.class.getClassLoader().getResourceAsStream("plant_catalog.xml");
        JAXBContext context = JAXBContext.newInstance(Catalog.class);

        Unmarshaller unmarshaller = context.createUnmarshaller();
        Catalog catalog = (Catalog) unmarshaller.unmarshal(plantStream);

        System.out.println();

        //TODO: wypisz wszystkie kwiatki
        //TODO: zsumuj ich ceny
        //TODO: light to enum
    }
}
