package pl.sda.xml;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class Plant {
    private String common;
    private String botanical;
    private Long zone;

    @XmlJavaTypeAdapter(PrinceAndCurrencyAdapter.class)
    private PriceAndCurrency price;
    private BigInteger availability;
    @XmlElementWrapper(name = "lights")
    @XmlElement(name = "light")
    private List<LightType> lights;

    public String getCommon() {
        return common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

    public List<LightType> getLights() {
        return lights;
    }

    public void setLights(List<LightType> lights) {
        this.lights = lights;
    }
}
