package pl.sda.xml;

import java.math.BigDecimal;

public class PriceAndCurrency {
    private String currencySign;
    private BigDecimal amount;

    public PriceAndCurrency(String currencySign, BigDecimal amount) {
        this.currencySign = currencySign;
        this.amount = amount;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
