package pl.sda.xml;

import javax.xml.bind.annotation.XmlEnumValue;

public enum  LightType {
    @XmlEnumValue("mostly shady") MOSTLY_SHADY,
    @XmlEnumValue("shade") SHADE,
    @XmlEnumValue("sun") SUN,
    @XmlEnumValue("shady") SHADY,
    @XmlEnumValue("sunned") SUNNED,
    @XmlEnumValue("sunny") SUNNY,
    @XmlEnumValue("mostly sunny") MOSTLY_SUNNY
}
